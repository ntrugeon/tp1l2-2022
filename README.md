# Correction du TP1

## Installation

```shell
git clone https://gitlab.univ-lr.fr/ntrugeon/tp1l2-2022.git tp1correction
cd tp1correction
composer install
symfony server:start
```

## Étude du code

Les éléments importants de ce TP sont :
- le découpage base.html.twig et index.html.twig
- la définition des routes dans le contrôleur et leurs utilisations dans les templates
- la gestion du formulaire
- la gestion des assets