<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route("/", name:"app_homepage")]
    public function index(Request $request, MailerInterface $mailer)
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) :
            $this->sendEmail($form->getData(),$mailer);
            // on revient sur la même page
            return $this->redirect($request->getUri());
        endif;
        return $this->render('default/index.html.twig', ['form_contact' => $form]);
    }

    #[Route("/produits/{id}",requirements:["id"=> "[1-9]\d*"], name:"app_produits")]
    public function produits($id=0) {
        return $this->render('default/produits.html.twig',['id'=>$id]);
    }

    #[Route("/histoire", name:"app_histoire")]
    public function histoire() {
        return $this->render('default/histoire.html.twig');
    }

    private function sendEmail($data, MailerInterface $mailer){
        $email = new Email();
        $email->from($data["email"])
          ->to('nicolas.trugeon@univ-lr.fr')
          ->subject($data["nom"])
          ->text($data["message"]);
        $mailer->send($email);
    }
}
